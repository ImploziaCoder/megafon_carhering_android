package com.carshering.implozia.megafon_carshering

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import com.carshering.implozia.megafon_carshering.fragment.CarFragment
import com.carshering.implozia.megafon_carshering.fragment.MenuFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var selectedFragment: Fragment = MenuFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeBottomNav()
        fragmentTransaction()
    }

    private fun initializeBottomNav(){
        bottomNavigation.setOnNavigationItemReselectedListener {
            BottomNavigationView.OnNavigationItemReselectedListener {
                when(it.itemId){
                    R.id.action_car -> selectedFragment = CarFragment()
                    R.id.action_menu -> selectedFragment = MenuFragment()
                }
                fragmentTransaction()
                true
            }
        }
    }

    private fun fragmentTransaction() {
        val fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frame_layout, selectedFragment)
        fragmentTransaction.commit()
    }
}
