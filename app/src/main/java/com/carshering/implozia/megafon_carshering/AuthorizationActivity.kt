package com.carshering.implozia.megafon_carshering

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.authorization_activity.*



/**
 * Created by Implozia on 19.05.2018.
 */
class AuthorizationActivity : AppCompatActivity(), Runnable {

    private var authorizationProgress: Int = 0
    private var handler = Handler()
    private val itsTimeUserName: Int = 5
    private val itsTimeActivityStart: Int = 6

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.authorization_activity)
        authorizate()
    }

    private fun authorizate() = buttonAuthorization.setOnClickListener {
        buttonAuthorization.startAnimation()
        handler.post(this)
    }

    override fun run() {
        handler.postDelayed(this, 1000)
        authorizationProgress++
        if (authorizationProgress == itsTimeUserName){
            userName.text = "Привет, Михаил"
        }
            if (authorizationProgress == itsTimeActivityStart) {
                startActivity(Intent(this,
                        MainActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                )
                finish()
                handler.removeCallbacksAndMessages(null)
            }
    }
}