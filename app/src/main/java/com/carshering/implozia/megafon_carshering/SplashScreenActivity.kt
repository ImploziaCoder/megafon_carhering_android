package com.carshering.implozia.megafon_carshering

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * Created by Implozia on 18.05.2018.
 */
class SplashScreenActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(Intent(this, AuthorizationActivity :: class.java))
        finish()
    }
}